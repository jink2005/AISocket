#ifndef AISUDPSOCKET_H
#define AISUDPSOCKET_H

#include <QObject>

QT_BEGIN_NAMESPACE
class QUdpSocket;
QT_END_NAMESPACE

class AISUdpTool : public QObject
{
    Q_OBJECT
public:
    explicit AISUdpTool(QObject *parent = 0);
    ~AISUdpTool();

    bool StartSocket(int port, QString &multiAddress);
    bool SendData(QString &message, QString &destAddress, quint16 dstPort);

signals:
    void receivedUdpData(QByteArray &byteArray);
public slots:
    void processPendingDatagrams();

private:
    QUdpSocket *udpSocket;
};

#endif // AISUDPSOCKET_H
