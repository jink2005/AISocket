#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class AISUdpTool;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void receivedUdpData(QByteArray &byteArray);

private slots:
    void on_tabWidget_currentChanged(int index);

    void on_pbUdpListen_clicked();

    void on_pbSendData_clicked();

    void on_pbClearHistory_clicked();

private:
    Ui::MainWindow *ui;
    AISUdpTool *udpTool;
};

#endif // MAINWINDOW_H
