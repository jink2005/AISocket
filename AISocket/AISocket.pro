#-------------------------------------------------
#
# Project created by QtCreator 2015-01-06T15:08:02
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AISocket
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    aisudptool.cpp

HEADERS  += mainwindow.h \
    aisudptool.h

FORMS    += mainwindow.ui
